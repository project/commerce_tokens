<?php

/**
 * @file
 * Builds placeholder replacement tokens for commerce-related data.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function commerce_tokens_token_info() {
  $types['commerce_currency'] = [
    'name' => t('Commerce currency'),
    'description' => t('Tokens related to commerce currency entity.'),
    'needs-data' => 'commerce_currency',
  ];
  $types['current-commerce-store'] = [
    'name' => t('Current Commerce Store'),
    'description' => t("Tokens related to the request's store."),
    'type' => 'commerce_store',
  ];
  $types['default-commerce-store'] = [
    'name' => t('Default Commerce Store'),
    'description' => t('Tokens related to the default store.'),
    'type' => 'commerce_store',
  ];
  $types['current-commerce-order'] = [
    'name' => t('Current Commerce Order'),
    'description' => t("Tokens related to the request's order."),
    'type' => 'commerce_order',
  ];
  $types['current-commerce-product'] = [
    'name' => t('Current Commerce Product'),
    'description' => t("Tokens related to the request's product."),
    'type' => 'commerce_product',
  ];
  $types['current-commerce-product-variation'] = [
    'name' => t('Current Commerce Product Variation'),
    'description' => t("Tokens related to the request's product variation."),
    'type' => 'commerce_product_variation',
  ];

  $commerce_store['id'] = [
    'name' => t('Store ID'),
    'description' => t('The unique ID of the store.'),
  ];
  $commerce_store['name'] = [
    'name' => t('Name'),
    'description' => t('The name of the store.'),
  ];
  $commerce_store['mail'] = [
    'name' => t('Email'),
    'description' => t('The email address of the store.'),
  ];
  $commerce_store['default_currency'] = [
    'name' => t('Default currency'),
    'description' => t('The default currency of the store.'),
    'type' => 'commerce_currency',
  ];

  $commerce_currency['id'] = [
    'name' => t('ID'),
    'description' => t('The unique ID of the currency.'),
  ];
  $commerce_currency['name'] = [
    'name' => t('Name'),
    'description' => t('The name of the currency.'),
  ];
  $commerce_currency['code'] = [
    'name' => t('Code'),
    'description' => t('The numeric code of the currency.'),
  ];
  $commerce_currency['symbol'] = [
    'name' => t('Symbol'),
    'description' => t('The symbol of the currency.'),
  ];
  $commerce_currency['fraction-digits'] = [
    'name' => t('Fraction digits'),
    'description' => t('The symbol of the currency.'),
  ];

  return [
    'types' => $types,
    'tokens' => [
      'commerce_store' => $commerce_store,
      'commerce_currency' => $commerce_currency,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function commerce_tokens_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  $token_service = \Drupal::token();
  if ($type == 'commerce_store' && !empty($data['commerce_store'])) {
    /** @var \Drupal\commerce_store\Entity\StoreInterface $store */
    $store = $data['commerce_store'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'id':
          // In the case of hook commerce_store_presave uid is not set yet.
          $replacements[$original] = $store->id() ?: t('not yet assigned');
          break;

        case 'name':
          $replacements[$original] = $store->getName();
          break;

        case 'mail':
          $replacements[$original] = $store->getEmail();
          break;
      }
    }

    if ($currency_tokens = $token_service->findWithPrefix($tokens, 'default_currency')) {
      $replacements += $token_service->generate('commerce_currency', $currency_tokens, ['commerce_currency' => $store->getDefaultCurrency()], $options, $bubbleable_metadata);
    }
  }
  elseif ($type == 'commerce_currency' && !empty($data['commerce_currency'])) {
    /** @var \Drupal\commerce_price\Entity\CurrencyInterface $currency */
    $currency = $data['commerce_currency'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'id':
        case 'code':
          $replacements[$original] = $currency->id();
          break;

        case 'name':
          $replacements[$original] = $currency->getName();
          break;

        case 'symbol':
          $replacements[$original] = $currency->getSymbol();
          break;

        case 'fraction-digits':
          $replacements[$original] = $currency->getFractionDigits();
          break;
      }
    }
  }
  elseif ($type == 'current-commerce-store') {
    $store = \Drupal::routeMatch()->getParameter('commerce_store');
    $bubbleable_metadata->addCacheContexts(['url']);
    $bubbleable_metadata->addCacheableDependency($store);
    $replacements += $token_service->generate('commerce_store', $tokens, ['commerce_store' => $store], $options, $bubbleable_metadata);
  }
  elseif ($type == 'default-commerce-store') {
    $store = \Drupal::service('commerce_store.default_store_resolver')->resolve();
    $bubbleable_metadata->addCacheableDependency($store);
    $replacements += $token_service->generate('commerce_store', $tokens, ['commerce_store' => $store], $options, $bubbleable_metadata);
  }
  elseif ($type == 'current-commerce-order') {
    $order = \Drupal::routeMatch()->getParameter('commerce_order');
    $bubbleable_metadata->addCacheContexts(['url']);
    $bubbleable_metadata->addCacheableDependency($order);
    $replacements += $token_service->generate('commerce_order', $tokens, ['commerce_order' => $order], $options, $bubbleable_metadata);
  }
  elseif ($type == 'current-commerce-product') {
    $product = \Drupal::routeMatch()->getParameter('commerce_product');
    $bubbleable_metadata->addCacheContexts(['url']);
    $bubbleable_metadata->addCacheableDependency($product);
    $replacements += $token_service->generate('commerce_product', $tokens, ['commerce_product' => $product], $options, $bubbleable_metadata);
  }
  elseif ($type == 'current-commerce-product-variation') {
    $product_variation = \Drupal::routeMatch()->getParameter('commerce_product_variation');
    $bubbleable_metadata->addCacheContexts(['url']);
    $bubbleable_metadata->addCacheableDependency($product_variation);
    $replacements += $token_service->generate('commerce_product_variation', $tokens, ['commerce_product_variation' => $product_variation], $options, $bubbleable_metadata);
  }

  return $replacements;
}
